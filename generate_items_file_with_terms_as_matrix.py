import argparse
import csv
import importlib.machinery
import logging
from datetime import datetime

import types


class ItemsFileWithTermMatrixGenerator:
    def __init__(self, settings_path):
        loader = importlib.machinery.SourceFileLoader(settings_path, settings_path)
        self.__settings = types.ModuleType(loader.name)
        loader.exec_module(self.__settings)

    def run_items_file_with_terms_matrix_generator(self, items_file, terms_file, output_file, id_column, name_column,
                                                   terms_column):

        try:
            self.generate_items_file_with_terms_matrix(items_file, terms_file, output_file, id_column, name_column,
                                                       terms_column)

        except Exception as ex:
            logging.exception("Exception: {}".format(ex.args))
            exit(-1)

    def generate_items_file_with_terms_matrix(self, items_file_path, terms_file_path, output_file_path, id_column_name,
                                              name_column_name, terms_column_name):

        items_file = open(items_file_path, 'r')
        items_file_reader = csv.DictReader(items_file)

        terms_file = open(terms_file_path, 'r')
        terms = terms_file.read().splitlines()

        with open(output_file_path, 'w') as items_out:
            wr = csv.writer(items_out, quoting=csv.QUOTE_NONNUMERIC)

            header_row = []
            header_row.append(self.__settings.ITEM_ID_COLUMN)
            header_row.append(self.__settings.ITEM_NAME_COLUMN)
            for term in terms:
                header_row.append(term)
            wr.writerow(header_row)

            for item in items_file_reader:
                wr.writerow(self.generate_item_row(item, terms, id_column_name, name_column_name, terms_column_name))

    def generate_item_row(self, item, terms, id_column_name, name_column_name, terms_column_name):
        # All categories plus two fields for id and description
        item_vector = [0] * (len(terms) + 2)
        item_vector[0] = int(item[id_column_name])
        item_vector[1] = item[name_column_name]
        # genres field in movies items is a pipe-separated field, we will transform it to an array
        for term in item[terms_column_name].split('|'):
            # displacement for id and title
            if term:
                index = terms.index(term) + 2
                item_vector[index] = 1
        return item_vector


def main():
    loader = importlib.machinery.SourceFileLoader("config/settings.py", "config/settings.py")
    settings = types.ModuleType(loader.name)
    loader.exec_module(settings)
    logging.basicConfig(filename=settings.LOG_PATH + str(datetime.today()), level=logging.INFO,
                        format=settings.LOG_FORMAT)

    parser = argparse.ArgumentParser(
        description='Generates a csv file that will contain one column as item_id, another as name and n columns for each term in the universe')
    parser.add_argument('--items_file', help='Path to items csv file.', required=True)
    parser.add_argument('--terms_file', help='Path to terms text file.', required=True)
    parser.add_argument('--output_file', help='Path to file that will include items with terms matrix', required=True)
    parser.add_argument('--id_column', help='Column name containing item identifier.', required=False,
                        default=settings.ITEM_ID_COLUMN)
    parser.add_argument('--name_column', help='Column name containing item name or description.', required=False,
                        default=settings.ITEM_NAME_COLUMN)
    parser.add_argument('--terms_column', help='Column name containing pipe-separated terms.', required=False,
                        default=settings.ITEM_TERMS_COLUMN)

    args = parser.parse_args()
    logging.info("Params <{}>".format(args))
    logging.info("Running ItemsFileWithTermMatrixGenerator")
    try:
        items_file_generator = ItemsFileWithTermMatrixGenerator("config/settings.py")
        items_file_generator.run_items_file_with_terms_matrix_generator(args.items_file, args.terms_file,
                                                                        args.output_file, args.id_column,
                                                                        args.name_column, args.terms_column)
    except Exception as ex_main:
        logging.error("ERROR: An error raised and the process ended: {}".format(str(ex_main)))
        exit(-1)

    logging.info("Process finished!")


if __name__ == '__main__':
    main()
