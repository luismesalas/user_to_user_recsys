import argparse
import csv
import importlib.machinery
import logging
from datetime import datetime

import numpy as np
import types
from lightfm import LightFM
from lightfm.evaluation import precision_at_k
from scipy.spatial import distance

from utils.generic_dataset_datamodeler import fetch_dataset


class UserToUserRecSysLightFM:
    def __init__(self, settings_path):
        loader = importlib.machinery.SourceFileLoader(settings_path, settings_path)
        self.__settings = types.ModuleType(loader.name)
        loader.exec_module(self.__settings)

    def run_recommender(self, ratings_file_path, items_with_terms_matrix_file_path, output_file_path, user_id_column,
                        item_id_column, rating_column, min_rating, distance_algorithm):

        try:

            logging.info("Running Movielens Modeler...")

            ratings_rows = self.get_ratings_row(ratings_file_path, user_id_column, item_id_column, rating_column)
            items, terms = self.get_items_and_terms(items_with_terms_matrix_file_path)

            # Constructing training and test dataset
            split_position = int(len(ratings_rows) * 0.8)
            training, test = ratings_rows[:split_position], ratings_rows[split_position:]

            # Load the dataset. Only five star ratings are treated as positive.
            data = fetch_dataset(training, test, items, terms, min_rating=float(min_rating))

            # Instantiate and train the model
            model = LightFM(loss=self.__settings.LFM_LOSS, learning_schedule=self.__settings.LFM_LEARNING_SCHEDULE,
                            no_components=self.__settings.LFM_NO_COMPONENTS, k=self.__settings.LFM_K,
                            n=self.__settings.LFM_N, learning_rate=self.__settings.LFM_LEARNING_RATE,
                            rho=self.__settings.LFM_RHO, epsilon=self.__settings.LFM_EPSILON,
                            item_alpha=self.__settings.LFM_ITEM_ALPHA, user_alpha=self.__settings.LFM_USER_ALPHA,
                            max_sampled=self.__settings.LFM_MAX_SAMPLED, random_state=self.__settings.LFM_RANDOM_STATE)
            model.fit(data['train'], epochs=self.__settings.LFM_EPOCHS, num_threads=self.__settings.LFM_NUM_THREADS,
                      verbose=self.__settings.LFM_VERBOSE)

            # Evaluate the trained model
            test_precision = precision_at_k(model, data['test'], k=self.__settings.LFM_PRECISION_K).mean()
            logging.info("Precision of model (1.0 represents maximum): {}".format(test_precision))

            # Getting users characteristic vectors
            whole_users_with_predictions = self.get_predictions_vector_for_each_user(model, data)

            # Generating distances file
            if distance_algorithm == self.__settings.COSINE_ALGORITHM:
                self.compute_cosine_distances_and_save_it_to_file(output_file_path, whole_users_with_predictions)
            elif distance_algorithm == self.__settings.EUCLIEDAN_ALGORITHM:
                self.compute_euclidean_distances_and_save_it_to_file(output_file_path, whole_users_with_predictions)
            else:
                self.compute_correlation_distances_and_save_it_to_file(output_file_path, whole_users_with_predictions)

        except Exception as ex:
            logging.exception("Exception: {}".format(ex.args))
            exit(-1)

    def compute_correlation_distances_and_save_it_to_file(self, output_file_path, whole_users_with_predictions):
        with open(output_file_path, 'w') as link_out:
            wr = csv.writer(link_out, quoting=csv.QUOTE_MINIMAL)
            final_file_header = []
            final_file_header.append('userId_left')
            final_file_header.append('userId_right')
            final_file_header.append('distance')

            wr.writerow(final_file_header)

            # Calculating distances and writing to file
            for user_left in whole_users_with_predictions:
                for user_right in whole_users_with_predictions:
                    if user_left[0] != user_right[0]:
                        curr_row = []
                        curr_row.append(user_left[0] + 1)
                        curr_row.append(user_right[0] + 1)
                        curr_row.append(distance.correlation(user_left[1], user_right[1]))
                        wr.writerow(curr_row)

    def compute_euclidean_distances_and_save_it_to_file(self, output_file_path, whole_users_with_predictions):
        with open(output_file_path, 'w') as link_out:
            wr = csv.writer(link_out, quoting=csv.QUOTE_MINIMAL)
            final_file_header = []
            final_file_header.append('userId_left')
            final_file_header.append('userId_right')
            final_file_header.append('distance')

            wr.writerow(final_file_header)

            # Calculating distances and writing to file
            for user_left in whole_users_with_predictions:
                for user_right in whole_users_with_predictions:
                    if user_left[0] != user_right[0]:
                        curr_row = []
                        curr_row.append(user_left[0] + 1)
                        curr_row.append(user_right[0] + 1)
                        curr_row.append(distance.euclidean(user_left[1], user_right[1]))
                        wr.writerow(curr_row)

    def compute_cosine_distances_and_save_it_to_file(self, output_file_path, whole_users_with_predictions):
        with open(output_file_path, 'w') as link_out:
            wr = csv.writer(link_out, quoting=csv.QUOTE_MINIMAL)
            final_file_header = []
            final_file_header.append('userId_left')
            final_file_header.append('userId_right')
            final_file_header.append('distance')

            wr.writerow(final_file_header)

            # Calculating distances and writing to file
            for user_left in whole_users_with_predictions:
                for user_right in whole_users_with_predictions:
                    if user_left[0] != user_right[0]:
                        curr_row = []
                        curr_row.append(user_left[0] + 1)
                        curr_row.append(user_right[0] + 1)
                        curr_row.append(distance.cosine(user_left[1], user_right[1]))
                        wr.writerow(curr_row)

    def get_predictions_vector_for_each_user(self, model, data):
        n_users, n_items = data['train'].shape
        whole_users_with_predictions = []
        for user_id in range(0, n_users):
            scores = model.predict(user_id, np.arange(n_items))
            whole_users_with_predictions.append([user_id, scores])
        return whole_users_with_predictions

    def get_ratings_row(self, ratings_file_path, user_id_column,
                        item_id_column, rating_column):
        ratings_file = open(ratings_file_path, 'r')
        ratings_file_reader = csv.DictReader(ratings_file)
        ratings_rows = []
        for rating in ratings_file_reader:
            # Subtract one from ids to shift to zero-based indexing
            ratings_rows.append(
                [int(rating[user_id_column]) - 1, int(rating[item_id_column]) - 1, float(rating[rating_column])])
        return ratings_rows

    def get_items_and_terms(self, items_with_terms_matrix_file_path):
        items_with_terms_matrix_file = open(items_with_terms_matrix_file_path, 'r')
        items_with_terms_matrix_file_reader = csv.DictReader(items_with_terms_matrix_file)

        terms = items_with_terms_matrix_file_reader.fieldnames[2:]

        items = []
        for item in items_with_terms_matrix_file_reader:
            row_values = []
            row_values.append(int(item[self.__settings.ITEM_ID_COLUMN]) - 1)
            row_values.append(item[self.__settings.ITEM_NAME_COLUMN])

            for term in terms:
                row_values.append(float(item[term]))

            items.append(row_values)

        return items, terms


def main():
    loader = importlib.machinery.SourceFileLoader("config/settings.py", "config/settings.py")
    settings = types.ModuleType(loader.name)
    loader.exec_module(settings)
    logging.basicConfig(filename=settings.LOG_PATH + str(datetime.today()), level=logging.INFO,
                        format=settings.LOG_FORMAT)
    parser = argparse.ArgumentParser(description='Generates an user-user similarity matrix in the specified path.')
    parser.add_argument('--ratings_file', help='Path to ratings csv file.', required=True)
    parser.add_argument('--items_file', help='Path to file containing items-terms matrix.', required=True)
    parser.add_argument('--output_file', help='Path to file that will include items with terms matrix', required=True)
    parser.add_argument('--user_id_column',
                        help='Column name containing user identifier. Default: {}'.format(settings.ITEM_ID_COLUMN),
                        required=False, default=settings.ITEM_ID_COLUMN)
    parser.add_argument('--item_id_column',
                        help='Column name containing item identifier. Default: {}'.format(settings.ITEM_NAME_COLUMN),
                        required=False, default=settings.ITEM_NAME_COLUMN)
    parser.add_argument('--rating_column',
                        help='Column name containing rating value. Default: {}'.format(settings.ITEM_TERMS_COLUMN),
                        required=False, default=settings.ITEM_TERMS_COLUMN)
    parser.add_argument('--min_rating', help='Minimun rating value to take in consideration.', required=False,
                        default=settings.DEFAULT_MIN_RATING)
    parser.add_argument('--distance_algorithm',
                        help='Distance algorithm to be used. Default: {}'.format(settings.CORRELATION_ALGORITHM),
                        choices=settings.ALGORITHM_CHOICES, required=False,
                        default=settings.CORRELATION_ALGORITHM)

    args = parser.parse_args()
    logging.info("Params <{}>".format(args))

    logging.info("Starting movielens collaborative filter recommender")
    try:
        user_to_user_recsys = UserToUserRecSysLightFM("config/settings.py")
        user_to_user_recsys.run_recommender(args.ratings_file, args.items_file, args.output_file, args.user_id_column,
                                            args.item_id_column, args.rating_column, args.min_rating, args.distance_algorithm)
    except Exception as ex_main:
        logging.error("ERROR: An error raised and the process ended: {}".format(str(ex_main)))
        exit(-1)

    logging.info("Process finished!")


if __name__ == '__main__':
    main()
