import numpy as np
import scipy.sparse as sp


def _get_dimensions(train_data, test_data, items_data):
    uids = set()
    iids = set()

    for item in train_data:
        uids.add(item[0])
        iids.add(item[1])

    for item in test_data:
        uids.add(item[0])
        iids.add(item[1])

    # This is for preventing we have itemIds without ratings
    for item in items_data:
        iids.add(item[0])

    rows = max(uids) + 1
    cols = max(iids) + 1

    return rows, cols


def _build_interaction_matrix(rows, cols, data, min_rating):
    mat = sp.lil_matrix((rows, cols), dtype=np.int32)

    for uid, iid, rating in data:
        if rating >= min_rating:
            mat[uid, iid] = rating

    return mat.tocoo()


def _parse_item_metadata(num_items, item_metadata_raw, genres_raw):
    id_feature_labels = np.empty(num_items, dtype=np.object)
    genre_feature_labels = np.array(genres_raw)

    id_features = sp.identity(num_items,
                              format='csr',
                              dtype=np.float32)
    genre_features = sp.lil_matrix((num_items, len(genre_feature_labels)),
                                   dtype=np.float32)

    for line in item_metadata_raw:

        if not line:
            continue

        iid = line[0]
        title = line[1]

        id_feature_labels[iid] = title

        # Omitting iid and title
        item_genres = [idx for idx, val in
                       enumerate(line[2:])
                       if int(val) > 0]

        for gid in item_genres:
            genre_features[iid, gid] = 1.0

    return (id_features, id_feature_labels,
            genre_features.tocsr(), genre_feature_labels)


def fetch_dataset(train_raw, test_raw, item_metadata_raw, genres_raw, min_rating=0.0):
    # Figure out the dimensions
    num_users, num_items = _get_dimensions(train_raw, test_raw, item_metadata_raw)

    # Load train interactions
    train = _build_interaction_matrix(num_users, num_items, train_raw, min_rating)

    # Load test interactions
    test = _build_interaction_matrix(num_users, num_items, test_raw, min_rating)

    assert train.shape == test.shape

    # Load metadata features
    (id_features, id_feature_labels,
     genre_features_matrix, genre_feature_labels) = _parse_item_metadata(num_items, item_metadata_raw, genres_raw)

    assert id_features.shape == (num_items, len(id_feature_labels))
    assert genre_features_matrix.shape == (num_items, len(genre_feature_labels))

    features = genre_features_matrix
    feature_labels = genre_feature_labels

    data = {'train': train,
            'test': test,
            'item_features': features,
            'item_feature_labels': feature_labels,
            'item_labels': id_feature_labels}

    return data
