import re

import unidecode


def normalize_text(text):
    text = unidecode.unidecode(text)
    text = text.lower()
    text = text.replace(' ', '_')
    text = text.replace('jr.', 'jr')
    text = text.replace('sr.', 'sr')
    text = text.replace('.', '_')
    text = text.replace(',', '_')
    text = text.replace('-', '_')
    text = text.replace(':', '_')
    text = text.replace(';', '_')
    text = text.replace('(', '_')
    text = text.replace(')', '_')
    text = text.replace('!', '_')
    text = text.replace('?', '_')
    text = text.replace('@', '_')
    text = text.replace('$', '_')
    text = text.replace('~', '_')
    text = text.replace('\t', '_')
    text = text.replace('\\', '_')
    text = text.replace('/', '_')
    text = text.replace('\'', '_')
    text = text.replace('"', '_')
    text = text.replace('`', '_')
    text = text.replace('|', '_')
    text = re.sub('_+', '_', text)
    return text
