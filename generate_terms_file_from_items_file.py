import argparse
import csv
import importlib.machinery
import logging
from datetime import datetime

import types


class TermsFileGenerator:
    def __init__(self, settings_path):
        loader = importlib.machinery.SourceFileLoader(settings_path, settings_path)
        self.__settings = types.ModuleType(loader.name)
        loader.exec_module(self.__settings)

    def run_terms_file_generator(self, items_file_path, output_file_path, column_name):

        try:
            self.generate_terms_file(items_file_path, output_file_path, column_name)

        except Exception as ex:
            logging.exception("Exception: {}".format(ex.args))
            exit(-1)

    def generate_terms_file(self, items_file_path, output_file_path, column_name):
        items_file = open(items_file_path, 'r')
        items_file_reader = csv.DictReader(items_file)

        terms = set()
        for item in items_file_reader:
            # terms field is pipe-separated field
            for term in item[column_name].split('|'):
                if term:
                    terms.add(term)

        output_file = open(output_file_path, 'w')
        for term in terms:
            output_file.write("%s\n" % term)


def main():
    loader = importlib.machinery.SourceFileLoader("config/settings.py", "config/settings.py")
    settings = types.ModuleType(loader.name)
    loader.exec_module(settings)
    logging.basicConfig(filename=settings.LOG_PATH + str(datetime.today()), level=logging.INFO,
                        format=settings.LOG_FORMAT)

    parser = argparse.ArgumentParser(
        description='Generates a text file that will contain distincts terms in a pipe-separated column from a CSV file')
    parser.add_argument('--items_file', help='Path items csv file.', required=True)
    parser.add_argument('--column_name', help='Column name that will contain pipe-separated terms', required=True)
    parser.add_argument('--output_file',
                        help='Path for to file that will include distinct terms for the complete dataset.',
                        required=True)

    args = parser.parse_args()
    logging.info("Params <{}>".format(args))
    logging.info("Running terms extractor")
    try:
        terms_file_generator = TermsFileGenerator("config/settings.py")
        terms_file_generator.run_terms_file_generator(args.items_file, args.output_file, args.column_name)
    except Exception as ex_main:
        logging.error("ERROR: An error raised and the process ended: {}".format(str(ex_main)))
        exit(-1)

    logging.info("Process finished!")


if __name__ == '__main__':
    main()
