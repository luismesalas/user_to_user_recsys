LOG_PATH = "/tmp/"
LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
TMDB_FILE_PATTERN = "movie_{}.json"
USER_ID_COLUMN = "userId"
ITEM_ID_COLUMN = "itemId"
RATING_COLUMN = "rating"
ITEM_NAME_COLUMN = "name"
ITEM_TERMS_COLUMN = "terms"
DEFAULT_MIN_RATING=5.0
CORRELATION_ALGORITHM="correlation"
COSINE_ALGORITHM="cosine"
EUCLIEDAN_ALGORITHM="euclidean"
ALGORITHM_CHOICES = [CORRELATION_ALGORITHM, COSINE_ALGORITHM ,EUCLIEDAN_ALGORITHM]

### Light FM configuration ###

## Model instantiation ##

# Loss function. Allowed values: 'logistic', 'warp', 'bpr' or 'warp-kos'
LFM_LOSS = 'warp'
# Learning rate scheduled. Allowed values: 'adagrad' or 'adadelta'
LFM_LEARNING_SCHEDULE = 'adadelta'
# The dimensionality of the feature latent embeddings (integer > 0)
LFM_NO_COMPONENTS = 10
# for k-OS training, the k-th positive example will be selected from the n positive examples sampled for every user (integer > 0)
LFM_K = 10
# for k-OS training, maximum number of positives sampled for each update (integer > 0)
LFM_N = 10
# initial learning rate for the adagrad learning schedule (float > 0.0)
LFM_LEARNING_RATE = 0.05
# moving average coefficient for the adadelta learning schedule (float 0.0 < rho < 1.0 )
LFM_RHO = 0.95
# conditioning parameter for the adadelta learning schedule (float >= 0.0)
LFM_EPSILON = 1e-8
# L2 penalty on item features (float >= 0.0)
LFM_ITEM_ALPHA = 0.0
# L2 penalty on user features (float >= 0.0)
LFM_USER_ALPHA = 0.0
#  maximum number of negative samples used during WARP fitting (integer > 1)
LFM_MAX_SAMPLED = 10
# The seed of the pseudo random number generator to use when shuffling the data and initializing the parameters (int seed, RandomState instance, or None)
LFM_RANDOM_STATE = 0

## Model training ##

# number of epochs to run (integer > 0)
LFM_EPOCHS=1
# number of parallel computation threads to use. Should not be higher than the number of physical cores (integer > 0)
LFM_NUM_THREADS=4
# whether to print progress message (boolean)
LFM_VERBOSE=False

## Training evaluation ##
LFM_PRECISION_K=3