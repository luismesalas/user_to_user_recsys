import argparse
import csv
import importlib.machinery
import json
import logging
import os
from datetime import datetime

import types

from utils.text_utils import normalize_text


class MoviesFileGenerator:
    def __init__(self, settings_path):
        loader = importlib.machinery.SourceFileLoader(settings_path, settings_path)
        self.__settings = types.ModuleType(loader.name)
        loader.exec_module(self.__settings)

    def run_terms_file_generator(self, links_file_path, tmdb_files, output_file):

        try:
            logging.info("Generating movies file from TMDB json files...")
            self.generate_csv_file(links_file_path, output_file, tmdb_files)

        except Exception as ex:
            logging.exception("Exception: {}".format(ex.args))
            exit(-1)

    def generate_csv_file(self, links_file_path, output_file, tmdb_files):
        links_file = open(links_file_path, 'r')
        links_file_reader = csv.DictReader(links_file)
        with open(output_file, 'w') as link_out:
            wr = csv.writer(link_out, quoting=csv.QUOTE_MINIMAL)

            header_row = []
            header_row.append(self.__settings.ITEM_ID_COLUMN)
            header_row.append(self.__settings.ITEM_NAME_COLUMN)
            header_row.append(self.__settings.ITEM_TERMS_COLUMN)

            wr.writerow(header_row)

            for link in links_file_reader:
                movieId = link['movieId']
                tmdb_movieId = link['tmdbId']

                tmdb_file_path = os.path.join(tmdb_files, self.__settings.TMDB_FILE_PATTERN.format(tmdb_movieId))
                if os.path.exists(tmdb_file_path):
                    curr_row = self.generate_row_from_json(movieId, tmdb_file_path)
                    wr.writerow(curr_row)

                else:
                    logging.warning(
                        "File not found for movieId '{}', expected file: {}.".format(movieId, tmdb_file_path))

    def generate_row_from_json(self, movieId, tmdb_file_path):
        tmdb_movie = json.load(open(tmdb_file_path, 'r'))
        normalized_terms = self.get_normalized_terms_from_movie(tmdb_movie)
        curr_row = []
        curr_row.append(movieId)
        curr_row.append('{} ({})'.format(tmdb_movie['title'], tmdb_movie['release_date']))
        curr_row.append('|'.join(normalized_terms))
        return curr_row

    def get_normalized_terms_from_movie(self, tmdb_movie):
        genres = tmdb_movie['genres']
        cast = tmdb_movie['cast']
        crew = tmdb_movie['crew']

        normalized_terms = []
        for genre in genres:
            normalized_terms.append(normalize_text('genre_{}'.format(genre['name'])))
        for cast_member in cast:
            normalized_terms.append(normalize_text('actor_{}'.format(cast_member['name'])))
        for crew_member in crew:
            if crew_member['job'] == 'Director':                normalized_terms.append(
                normalize_text('director_{}'.format(crew_member['name'])))
            if crew_member['job'] == 'Producer':                normalized_terms.append(
                normalize_text('producer_{}'.format(crew_member['name'])))
            if crew_member['job'] == 'Writer':                normalized_terms.append(
                normalize_text('writer_{}'.format(crew_member['name'])))
        return normalized_terms


def main():
    loader = importlib.machinery.SourceFileLoader("config/settings.py", "config/settings.py")
    settings = types.ModuleType(loader.name)
    loader.exec_module(settings)
    logging.basicConfig(filename=settings.LOG_PATH + str(datetime.today()), level=logging.INFO,
                        format=settings.LOG_FORMAT)

    parser = argparse.ArgumentParser(
        description='Generates movies CSV file from TMDB json files')
    parser.add_argument('--tmdb_files', help='Path for reading json files downloaded from TMDB.', required=True)
    parser.add_argument('--links_file', help='Path to links.csv file.', required=True)
    parser.add_argument('--output_file', help='Path for to file that will include movielens movieId with terms.',
                        required=True)

    args = parser.parse_args()
    logging.info("Params <{}>".format(args))
    logging.info("Starting module for creating a new moviles file from TMDB json files")
    try:
        movies_file_generator = MoviesFileGenerator("config/settings.py")
        movies_file_generator.run_terms_file_generator(args.links_file, args.tmdb_files, args.output_file)
    except Exception as ex_main:
        logging.error("ERROR: An error raised and the process ended: {}".format(str(ex_main)))
        exit(-1)

    logging.info("Process finished!")


if __name__ == '__main__':
    main()
