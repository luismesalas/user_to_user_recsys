# User to user RecSys

This project is built in order to create a standard tool for measuring distances betweem two users based on its ratings on a items dataset.

In order to do that, we tried to use [tmdb_dowloader project](https://gitlab.com/luismesalas/tmdb_downloader) for creating a richer model of terms for each movie contained on [movielens dataset](https://grouplens.org/datasets/movielens/)

## Setting up

There is a `requirements.txt` file in this repository.

You can easily install the dependencies like this:

```bash
pip install -r requirements.txt
```

## Scripts

We've made several scripts for transforming TMDB and movielens dataset obtained data suitable for being used with [LightFM](https://github.com/lyst/lightfm).

### generate_movies_file_from_tmdb_files.py

This scripts is for generating a movies files containing a list of movieId, title and terms obtained from TMDB downloaded files.

These are the options for executing this script:

```bash
usage: generate_movies_file_from_tmdb_files.py [-h] --tmdb_files TMDB_FILES
                                               --links_file LINKS_FILE
                                               --output_file OUTPUT_FILE

Generates movies CSV file from TMDB json files

optional arguments:
  -h, --help            show this help message and exit
  --tmdb_files TMDB_FILES
                        Path for reading json files downloaded from TMDB.
  --links_file LINKS_FILE
                        Path to links.csv file.
  --output_file OUTPUT_FILE
                        Path for to file that will include movielens movieId
                        with terms.
```

Every parameter it's required. Here it is a sample execution command:

```bash
python generate_movies_file_from_tmdb_files.py --tmdb_files=/home/luismesalas/tmdb_movies/ --links_file=/home/luismesalas/ml-latest/links.csv --output_file=/home/luismesalas/ml-latest/movies_tmdb.csv
```

### generate_terms_file_from_items_file.py

This scripts is for generating a plain text file containing a bag of words representing the possible terms contained in the third csv column of the previous generated file. One per each line.

These are the options for executing this script:

```bash
usage: generate_terms_file_from_items_file.py [-h] --items_file ITEMS_FILE
                                              --column_name COLUMN_NAME
                                              --output_file OUTPUT_FILE

Generates a text file that will contain distincts terms in a pipe-separated
column from a CSV file

optional arguments:
  -h, --help            show this help message and exit
  --items_file ITEMS_FILE
                        Path items csv file.
  --column_name COLUMN_NAME
                        Column name that will contain pipe-separated terms
  --output_file OUTPUT_FILE
                        Path for to file that will include distinct terms for
                        the complete dataset.
```

Every parameter it's required. Here it is a sample execution command:

```bash
python generate_terms_file_from_items_file.py --items_file=/home/luismesalas/ml-latest/movies_tmdb.csv --column_name=terms --output_file=/home/luismesalas/ml-latest/terms_tmdb.txt
```

And here it is another command for using it with `movielens` movies csv file:

```bash
python generate_terms_file_from_items_file.py --items_file=/home/luismesalas/ml-latest/movies.csv --column_name=genres --output_file=/home/luismesalas/ml-latest/terms.txt
```

### generate_items_file_with_terms_as_matrix.py

This script will transform previous movies file into a file with te following format: movieId, name, term1, term2 ... termN. The terms column are representing if an item (a movie in this case) have that term. 1 if true, o if false.


These are the options for executing this script:

```bash
usage: generate_items_file_with_terms_as_matrix.py [-h] --items_file
                                                   ITEMS_FILE --terms_file
                                                   TERMS_FILE --output_file
                                                   OUTPUT_FILE
                                                   [--id_column ID_COLUMN]
                                                   [--name_column NAME_COLUMN]
                                                   [--terms_column TERMS_COLUMN]

Generates a csv file that will contain one column as item_id, another as name
and n columns for each term in the universe

optional arguments:
  -h, --help            show this help message and exit
  --items_file ITEMS_FILE
                        Path to items csv file.
  --terms_file TERMS_FILE
                        Path to terms text file.
  --output_file OUTPUT_FILE
                        Path to file that will include items with terms matrix
  --id_column ID_COLUMN
                        Column name containing item identifier.
  --name_column NAME_COLUMN
                        Column name containing item name or description.
  --terms_column TERMS_COLUMN
                        Column name containing pipe-separated terms.
```

Every parameters are mandatory except for column names that takes 'itemId', 'name' and 'terms' values by default.

Here it is a sample for TMDB movies file:

```bash
python generate_items_file_with_terms_as_matrix.py --items_file=/home/luismesalas/ml-latest/movies_tmdb.csv --terms_file=/home/luismesalas/ml-latest/terms_tmdb.txt --output_file=/home/luismesalas/ml-latest/items_terms_matrix_tmdb.csv
```

And here it is the other sample for movielens dataset movies file:

```bash
python generate_items_file_with_terms_as_matrix.py --items_file=/home/luismesalas/ml-latest/movies.csv --terms_file=/home/luismesalas/ml-latest/terms.txt --output_file=/home/luismesalas/ml-latest/items_terms_matrix.csv --id_colum=movieId --name_colum=title --terms_colum=genres
```

### user_to_user_recsys.py

And finally, this script will build a model and generate a distance file for each pair of users.

First of all, you may want to configure `settings.py` file for specifying model build and train parameters:

```python
### Light FM configuration ###

## Model instantiation ##

# Loss function. Allowed values: 'logistic', 'warp', 'bpr' or 'warp-kos'
LFM_LOSS = 'logistic'
# Learning rate scheduled. Allowed values: 'adagrad' or 'adadelta'
LFM_LEARNING_SCHEDULE = 'adagrad'
# The dimensionality of the feature latent embeddings (integer > 0)
LFM_NO_COMPONENTS = 10
# for k-OS training, the k-th positive example will be selected from the n positive examples sampled for every user (integer > 0)
LFM_K = 5
# for k-OS training, maximum number of positives sampled for each update (integer > 0)
LFM_N = 10
# initial learning rate for the adagrad learning schedule (float > 0.0)
LFM_LEARNING_RATE = 0.05
# moving average coefficient for the adadelta learning schedule (float 0.0 < rho < 1.0 )
LFM_RHO = 0.95
# conditioning parameter for the adadelta learning schedule (float >= 0.0)
LFM_EPSILON = 1e-6
# L2 penalty on item features (float >= 0.0)
LFM_ITEM_ALPHA = 0.0
# L2 penalty on user features (float >= 0.0)
LFM_USER_ALPHA = 0.0
#  maximum number of negative samples used during WARP fitting (integer > 1)
LFM_MAX_SAMPLED = 10
# The seed of the pseudo random number generator to use when shuffling the data and initializing the parameters (int seed, RandomState instance, or None)
LFM_RANDOM_STATE = None

## Model training ##

# number of epochs to run (integer > 0)
LFM_EPOCHS=1
# number of parallel computation threads to use. Should not be higher than the number of physical cores (integer > 0)
LFM_NUM_THREADS=4
# whether to print progress message (boolean)
LFM_VERBOSE=0
```

Then, you can run the script with the following parameters configuration:

```bash
usage: user_to_user_recsys.py [-h] --ratings_file RATINGS_FILE --items_file
                              ITEMS_FILE --output_file OUTPUT_FILE
                              [--user_id_column USER_ID_COLUMN]
                              [--item_id_column ITEM_ID_COLUMN]
                              [--rating_column RATING_COLUMN]
                              [--min_rating MIN_RATING]
                              [--distance_algorithm {correlation,cosine,euclidean}]

Generates an user-user similarity matrix in the specified path.

optional arguments:
  -h, --help            show this help message and exit
  --ratings_file RATINGS_FILE
                        Path to ratings csv file.
  --items_file ITEMS_FILE
                        Path to file containing items-terms matrix.
  --output_file OUTPUT_FILE
                        Path to file that will include items with terms matrix
  --user_id_column USER_ID_COLUMN
                        Column name containing user identifier. Default:
                        itemId
  --item_id_column ITEM_ID_COLUMN
                        Column name containing item identifier. Default: name
  --rating_column RATING_COLUMN
                        Column name containing rating value. Default: terms
  --min_rating MIN_RATING
                        Minimun rating value to take in consideration.
  --distance_algorithm {correlation,cosine,euclidean}
                        Distance algorithm to be used. Default: correlation
```

This is a sample execution command for this script:

```bash
python user_to_user_recsys.py --ratings_file=/home/luismesalas/ml-latest-small/ratings.csv --items_file=/home/luismesalas/ml-latest-small/items_terms_matrix.csv --output_file=/home/luismesalas/ml-latest-small/distances_exp2.csv --user_id_column=userId --item_id_column=movieId --rating_column=rating --min_rating=2.0
```

## Greetings

[LightFM](https://github.com/lyst/lightfm) has helped me so hard in this research, so here it is a reference to her work:

```
@inproceedings{DBLP:conf/recsys/Kula15,
  author    = {Maciej Kula},
  editor    = {Toine Bogers and
               Marijn Koolen},
  title     = {Metadata Embeddings for User and Item Cold-start Recommendations},
  booktitle = {Proceedings of the 2nd Workshop on New Trends on Content-Based Recommender
               Systems co-located with 9th {ACM} Conference on Recommender Systems
               (RecSys 2015), Vienna, Austria, September 16-20, 2015.},
  series    = {{CEUR} Workshop Proceedings},
  volume    = {1448},
  pages     = {14--21},
  publisher = {CEUR-WS.org},
  year      = {2015},
  url       = {http://ceur-ws.org/Vol-1448/paper4.pdf},
}
```
